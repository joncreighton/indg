The first thing to implement is the gameplay logic.  This could be done in a command-line program, but I'll host it in Unity because that's what I'll use to do the presentation side later on.

Firstly get the game loop working for two human players then add the AI logic for playing against the computer.

**AI**

From my brief analysis of the game it appears that the player who has a move when there is 5 pieces on the board will alway lose.

For instance a decision tree for the last 7 pieces might be.  Here players A and B have the number of pieces that they remove indicated in the square brackets:

7:
  A[1] - 6:
    B[1] - 5:
      A[1] - 4:
        B[1] - 3:
          A[1] - 2:
            B[1] - 1:
              A[1] - LOSE
            B[2] - LOSE
          A[2] - 1:
            B[1] - LOSE
        B[2] - 2:
          A[1] - 1:
            B[1] - LOSE
          A[2] - LOSE
        B[3] - 1:
          A[1] - LOSE
      A[2] - 3:
        B[1] - 2:
          A[1] - 1:
            B[1] - LOSE
          A[2] - LOSE
        B[2] - 1:
          A[1] - LOSE
      A[3] - 2:
        B[1] - 1:
          A[1] - LOSE
        B[2] - LOSE
    B[2] - 4:
      A[1] - 3:
        B[1] - 2:
          A[1] - 1:
            B[1] - LOSE
          A[2] - LOSE
        B[2] - 1:
          A[1] - LOSE
      A[2] - 2:
        B[1] - 1:
          A[1] - LOSE
        B[2] - LOSE
      A[3] - 1:
        B[1] - LOSE
    B[3] - 3:
      A[1] - 2:
        B[1] - 1:
          A[1] - LOSE
        B[2] - LOSE
      A[2] - 1:
        B[1] - LOSE
  A[2] - 5:
    ...
  A[3] - 4:
    ...

For the AI I take a brute-force approach and build up a decision tree like this for all of the moves.  The number of winning posibilities for both the computer and the player are kept track of while the tree is being built and propagated back up to the root.  The best move is one that has the highest ratio between these values.  The first few turns can be completely random, but the earlier in the game that we use this tree then the more likely that the computer will be able to find a winning solution.

Currently it is very slow to generate the tree for more than 20 or so moves.  It would be possible to speed this up in many ways; for instance you could switch to a iterative algorithm rather than a recursive one or alternatively the nodes don't have to be created as objects at all, saving the memory allocation overhead.  The current slow implementation was the clearest solution that I came up with and it has the benefit of allowing the ruleset to be changed easily.


**PRESENTATION**

The flow of the game immediately made me think of children's games which involve picking the petals off flowers (loves me, loves me not etc).  So I thought a good presentation would be silhouettes of flowers against a neutral background.

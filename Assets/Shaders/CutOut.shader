﻿Shader "Custom/CutOut" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Colour ("Colour", Color) = (1.0, 1.0, 1.0, 1.0)
	}
	SubShader {
		Tags { "Queue"="AlphaTest" "RenderType"="TransparentCutout" }

		Cull Off
		LOD 200
		
		CGPROGRAM
		#pragma surface surf FlatColour alphatest:_Cutoff


		fixed4 LightingFlatColour(SurfaceOutput s, fixed3 lightDir, half3 viewDir, fixed atten)
		{
			return fixed4(s.Albedo, s.Alpha);
		}

		fixed4 LightingFlatColour_PrePass(SurfaceOutput s, half4 light)
		{
			return fixed4(s.Albedo, s.Alpha);
		}

		sampler2D _MainTex;
		float4 _Colour;

		struct Input {
			float2 uv_MainTex;
		};

		void surf (Input IN, inout SurfaceOutput o)
		{
			fixed4 alphaTex = tex2D(_MainTex, IN.uv_MainTex);

			clip(alphaTex.g - 0.5);

			o.Albedo = _Colour.rgb;
			o.Alpha = alphaTex.g;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}

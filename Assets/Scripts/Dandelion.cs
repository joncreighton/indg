﻿using UnityEngine;
using System.Collections;

public class Dandelion : MonoBehaviour
{
    float _animationOffset;
    float _frequency = 0.1f;
    float _magnitude = 0.1f;
    bool _isFull = true;
    SphereCollider _headSphere;
    Renderer _renderer;

	// Use this for initialization
	void Start ()
    {
        _animationOffset = Random.Range(0.0f, 100.0f);
        _headSphere = GetComponentInChildren<SphereCollider>();
        _renderer = GetComponentInChildren<Renderer>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        float rotation = Mathf.Sin(Time.time * _frequency + _animationOffset) * _magnitude;
        Vector3 up = new Vector3(rotation, 1.0f, 0.0f);
        up.Normalize();

        transform.up = up;
	}

    public bool IsFull
    {
        get { return _isFull; }
        set { _isFull = value; }
    }

    public Renderer Renderer
    {
        get { return _renderer; }
    }

    public SphereCollider SphereCollider
    {
        get { return _headSphere; }
    }
}

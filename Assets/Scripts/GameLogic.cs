﻿using UnityEngine;
using System.Collections.Generic;

public class Player
{
    public Player(bool human)
    {
        _piecesTaken = 0;
        _human = human;
    }

    public int _piecesTaken;
    public bool _human;
}

public class GameLogic : MonoBehaviour
{
    enum State
    {
        Menu,
        Playing,
        Finished
    }

    State _state;
    int _pieces;
    int _prevPieces;
    int _piecesTaken;
    int _activePlayer;
    Player[] _players = new Player[2];
    DandelionField _dandelions;

    public GUIStyle _titleStyle;
    public GUIStyle _buttonStyle;
    public float _titleSize = 0.05f;
    public float _buttonSize = 0.03f;

    float _fogFadeStart;
    float _fogStartValue;

	// Use this for initialization
	void Start ()
    {
        _dandelions = FindObjectOfType<DandelionField>();

        _pieces = _dandelions._dandelionMax;
        _prevPieces = _pieces;
        _piecesTaken = 0;
        _state = State.Menu;

        _titleStyle = new GUIStyle();
        _titleStyle.fontSize = 20;
        _titleStyle.fontStyle = FontStyle.Bold;
        _titleStyle.normal.textColor = Color.black;
        _titleStyle.alignment = TextAnchor.MiddleCenter;

        _buttonStyle = new GUIStyle();
        _buttonStyle.fontSize = 20;
        _buttonStyle.fontStyle = FontStyle.Bold;
        _buttonStyle.alignment = TextAnchor.MiddleCenter;
        _buttonStyle.normal.textColor = new Color(0.8f, 0.2f, 0.2f);

        _dandelions.Reset(_pieces);

        _fogFadeStart = Time.time;
        _fogStartValue = -70.0f;
    }
	
    // Create a rect that is relative to the screen resolution, useful for the GUI.
    Rect RelativeRect(float x, float y, float w, float h)
    {
        return new Rect(x * Screen.width, y * Screen.height, w * Screen.width, h * Screen.height);
    }

    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Debug.DrawLine(ray.origin, ray.origin + ray.direction * 1000.0f);


        if (_state == State.Playing)
        {
            if (_players[_activePlayer]._human)
            {
                if (_piecesTaken == 3)
                    NextPlayer();

                if (Input.GetMouseButtonDown(0))
                {
                    RaycastHit hitInfo;
                    if (Physics.Raycast(ray, out hitInfo))
                    {
                        Transform Xform = hitInfo.collider.gameObject.transform.parent.parent;
                        Dandelion dandelion = Xform.GetComponent<Dandelion>();

                        if (dandelion != null)
                        {
                            if (dandelion.IsFull)
                            {
                                _dandelions.BurstDandelion(dandelion);
                                ++_piecesTaken;
                                --_pieces;
                                ++_players[_activePlayer]._piecesTaken;

                                if (_pieces == 0)
                                {
                                    _fogFadeStart = Time.time;
                                    _fogStartValue = RenderSettings.fogStartDistance;
                                    _state = State.Finished;
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                ComputerMove();
            }
        }
    }

    void OnGUI()
    {
        _titleStyle.fontSize = (int)(_titleSize * Screen.height);
        _buttonStyle.fontSize = (int)(_buttonSize * Screen.height);

        switch (_state)
        {
            case State.Menu:
                StartGUI();
                break;

            case State.Playing:
                if (_players[_activePlayer]._human)
                    PlayerGUI();
                break;

            case State.Finished:
                FinishedGUI();
                break;
        }
    }
    
    void StartGUI()
    {
        float fogFade = Mathf.Min((Time.time - _fogFadeStart) / 3.0f, 1.0f);
        RenderSettings.fogStartDistance = Mathf.Lerp(_fogStartValue, -70.0f, fogFade);

        GUI.Label(RelativeRect(0.1f, 0.01f, 0.8f, 0.05f), "Last One Out Loses", _titleStyle);

        if (GUI.Button(RelativeRect(0.35f, 0.1f, 0.3f, 0.05f), "Player Goes First", _buttonStyle))
            StartGame(true, false);

        if (GUI.Button(RelativeRect(0.35f, 0.17f, 0.3f, 0.05f), "Computer Goes First", _buttonStyle))
            StartGame(false, true);

        if (GUI.Button(RelativeRect(0.35f, 0.24f, 0.3f, 0.05f), "Two Player Game", _buttonStyle))
            StartGame(true, true);

        if (GUI.Button(RelativeRect(0.35f, 0.31f, 0.3f, 0.05f), "Quit", _buttonStyle))
            Application.Quit();
    }

    void PlayerGUI()
    {
        float fogFade = Mathf.Min((Time.time - _fogFadeStart) / 3.0f, 1.0f);
        RenderSettings.fogStartDistance = Mathf.Lerp(_fogStartValue, 70.0f, fogFade);

        if (_piecesTaken < 3)
        {
            string playerString = (_activePlayer == 0) ? "One" : "Two";
            string[] possiblePicks = { "one more dandelion.", "two more dandelions.", "up to three dandelions." };

            int picks = Mathf.Min((2 - _piecesTaken), _pieces - 1);

            GUI.Label(RelativeRect(0.1f, 0.01f, 0.8f, 0.05f), "Player " + playerString + ", pick " + possiblePicks[picks], _titleStyle);

            if (GUI.Button(RelativeRect(0.89f, 0.01f, 0.1f, 0.05f), "QUIT", _buttonStyle))
            {
                _fogFadeStart = Time.time;
                _fogStartValue = RenderSettings.fogStartDistance;
                _state = State.Menu;
            }

            if (_piecesTaken > 0)
            {
                if (GUI.Button(RelativeRect(0.01f, 0.01f, 0.1f, 0.05f), "PASS", _buttonStyle))
                {
                    NextPlayer();
                }
            }
        }
    }
   
    void FinishedGUI()
    {
        float fogFade = Mathf.Min((Time.time - _fogFadeStart) / 3.0f, 1.0f);
        RenderSettings.fogStartDistance = Mathf.Lerp(_fogStartValue, -70.0f, fogFade);

        string loserName = _players[_activePlayer]._human ? "Player " + (_activePlayer + 1) : "The Computer";
        GUI.Label(RelativeRect(0.1f, 0.15f, 0.8f, 0.05f), loserName + " lost", _titleStyle);

        if (GUI.Button(RelativeRect(0.35f, 0.24f, 0.3f, 0.05f), "Play Again", _buttonStyle))
            _state = State.Menu;

        if (GUI.Button(RelativeRect(0.35f, 0.31f, 0.3f, 0.05f), "Quit", _buttonStyle))
            Application.Quit();
    }

    void StartGame(bool human0, bool human1)
    {
        _players[0] = new Player(human0);
        _players[1] = new Player(human1);
        _activePlayer = 0;
        _pieces = _dandelions._dandelionMax;
        _prevPieces = _pieces;
        _piecesTaken = 0;
        _state = State.Playing;
        _fogFadeStart = Time.time;

        if (_dandelions.FullDandelions.childCount != _pieces)
        {
            _dandelions.Reset(_pieces);
        }
    }

    void NextPlayer()
    {
        if (_pieces == 1)
        {
            _fogFadeStart = Time.time;
            _fogStartValue = RenderSettings.fogStartDistance;
            _state = State.Finished;
        }

        _piecesTaken = 0;
        _activePlayer = (_activePlayer + 1) % 2;
    }

    void ComputerMove()
    {
        int count = 0;

        if (_pieces > 20)
            count = Random.Range(1, 4);
        else
            count = ComputeMove(_activePlayer, _pieces);

        _pieces -= count;
        _players[_activePlayer]._piecesTaken += count;

        for (int i = 0; i < count; ++i)
        {
            _dandelions.BurstDandelion(_dandelions.RandomDandelion());
        }

        NextPlayer();
    }

    int ComputeMove(int aiPlayer, int pieces)
    {
        int otherPlayer = (aiPlayer + 1) % 2;
        DecisionNode tree = DecisionNode.Create(aiPlayer, otherPlayer, pieces, 0);
        int bestChoice = 0;
        float maxFitness = 0.0f;

        Debug.Log("Pieces:" + pieces + ", All Permutations:" + (tree._playerWins + tree._computerWins) + ", Player Wins:" + tree._playerWins + ", Computer Wins:" + tree._computerWins);

        foreach (DecisionNode node in tree._children)
        {
            float fitness = 10000.0f;   // Avoid the divide-by-zero by returning a high fitness value when the player can't win.

            if (node._playerWins != 0)
                fitness = (float)node._computerWins / (float)node._playerWins;

            Debug.Log("Take:" + node._piecesTaken + ", Leaving:" + (pieces - node._piecesTaken) + ", Player:" + node._playerWins + ", Computer:" + node._computerWins + ", Fitness:" + fitness);

            if (fitness >= maxFitness)
            {
                maxFitness = fitness;
                bestChoice = node._piecesTaken;
            }
        }

        return bestChoice;
    }
}

class DecisionNode
{
    DecisionNode(int piecesTaken)
    {
        _piecesTaken = piecesTaken;
        _computerWins = 0;
        _playerWins = 0;
        _children = new List<DecisionNode>(3);
    }

    static public DecisionNode Create(int aiPlayer, int nodePlayer, int piecesBefore, int piecesTaken)
    {
        DecisionNode node = new DecisionNode(piecesTaken);

        int piecesAfter = piecesBefore - piecesTaken;

        if (piecesAfter == 0)
        {
            // If this move results in 0 pieces left then increment the win counter for the other player.
            if (nodePlayer != aiPlayer)
                ++node._computerWins;
            else
                ++node._playerWins;
        }
        else
        {
            // Recurse into up to 3 children, one for each possible move.
            int nextPlayer = (nodePlayer + 1) % 2;

            for (int toTake = 1; toTake < 4; ++toTake)
            {
                if (piecesAfter >= toTake)
                {
                    DecisionNode child = Create(aiPlayer, nextPlayer, piecesAfter, toTake);
                    node._children.Add(child);
                    node._computerWins += child._computerWins;
                    node._playerWins += child._playerWins;
                }
            }
        }

        return node;
    }

    public int _piecesTaken;
    public List<DecisionNode> _children;
    public int _computerWins;
    public int _playerWins;
}

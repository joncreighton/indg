﻿using UnityEngine;
using System.Collections.Generic;

public class DandelionField : MonoBehaviour
{
    public int _dandelionMax = 20;
    public int _seedMax = 40;
    public int _seedsInDandelion = 20;
    public GameObject _dandelionPrefab;
    public GameObject _seedPrefab;
    public Material _fullDandelion;
    public Material _emptyDandelion;
    public Perlin _perlin;
    GameObject _inactiveDandelions;
    GameObject _inactiveSeeds;
    GameObject _fullDandelions;
    GameObject _burstDandelions;
    GameObject _seeds;
    GameObject[] _dandelionPool;
    GameObject[] _seedPool;

	// Create a pool of the maximum number of dandelions and seeds.
    void Awake()
    {
        BoxCollider box = GetComponent<BoxCollider>();

        _perlin = new Perlin(15485863);

        _inactiveDandelions = new GameObject();
        _inactiveDandelions.name = "Inactive Dandelions";
        _inactiveDandelions.SetActive(false);
        _inactiveDandelions.transform.parent = transform;
        _inactiveSeeds = new GameObject();
        _inactiveSeeds.name = "Inactive Seeds";
        _inactiveSeeds.SetActive(false);
        _inactiveSeeds.transform.parent = transform;
        _fullDandelions = new GameObject();
        _fullDandelions.name = "Full Dandelions";
        _fullDandelions.transform.parent = transform;
        _burstDandelions = new GameObject();
        _burstDandelions.name = "Burst Dandelions";
        _burstDandelions.transform.parent = transform;
        _seeds = new GameObject();
        _seeds.name = "Seeds";
        _seeds.transform.parent = transform;

        if (_dandelionPrefab != null)
        {
            _dandelionPool = new GameObject[_dandelionMax];
            for (int i = 0; i < _dandelionMax; ++i)
            {
                _dandelionPool[i] = (GameObject)Instantiate(_dandelionPrefab);
                _dandelionPool[i].transform.parent = _inactiveDandelions.transform;
            }
        }

        if (_seedPrefab != null)
        {
            _seedPool = new GameObject[_seedMax];

            for (int i = 0; i < _seedMax; ++i)
            {
                _seedPool[i] = (GameObject)Instantiate(_seedPrefab);
                _seedPool[i].transform.parent = _inactiveSeeds.transform;
            }
        }
    }

    public void Reset(int dandelionCount)
    {
        BoxCollider box = GetComponent<BoxCollider>();

        // Move the dandelions and seeds back to the inactive pool.
        while (_fullDandelions.transform.childCount > 0)
        {
            _fullDandelions.transform.GetChild(0).parent = _inactiveDandelions.transform;
        }

        while (_burstDandelions.transform.childCount > 0)
        {
            Transform Xform = _burstDandelions.transform.GetChild(0);
            Dandelion dandelion = Xform.GetComponent<Dandelion>();

            Xform.parent = _inactiveDandelions.transform;
            dandelion.Renderer.material = _fullDandelion;
            dandelion.IsFull = true;
            dandelion.SphereCollider.radius = 1.0f;

        }

        while (_seeds.transform.childCount > 0)
        {
            _seeds.transform.GetChild(0).parent = _inactiveSeeds.transform;
        }

        // Populate the dandelions with the number requested.
        dandelionCount = Mathf.Min(dandelionCount, _inactiveDandelions.transform.childCount);

        float xStep = (box.bounds.max.x - box.bounds.min.x) / dandelionCount;
        float xValue = box.bounds.min.x + xStep * 0.5f;

        while (_fullDandelions.transform.childCount < dandelionCount)
        {
            Transform dandelion = _inactiveDandelions.transform.GetChild(0);
            dandelion.parent = _fullDandelions.transform;
            dandelion.position = new Vector3(xValue, Random.Range(box.bounds.min.y, box.bounds.max.y), Random.Range(box.bounds.min.z, box.bounds.max.z));

            xValue += xStep;
        }
    }

    public void KillSeed(Seed seed)
    {
        seed.transform.parent = _inactiveSeeds.transform;
    }

    public Dandelion RandomDandelion()
    {
        Transform dandelion =_fullDandelions.transform.GetChild(0);
        dandelion.parent = _burstDandelions.transform;

        return dandelion.GetComponent<Dandelion>();
    }

    public Transform FullDandelions
    {
        get
        {
            return _fullDandelions.transform;
        }
    }

    public void BurstDandelion(Dandelion dandelion)
    {
        dandelion.IsFull = false;
        dandelion.SphereCollider.radius = 0.0f;
        dandelion.transform.parent = _burstDandelions.transform;
        Transform dandelionHead = dandelion.SphereCollider.gameObject.transform;

        dandelion.Renderer.material = _emptyDandelion;

        List<Transform> seeds = new List<Transform>(_seedsInDandelion);

        // Don't have enough inactive seeds, need to recycle some.
        if (_inactiveSeeds.transform.childCount < _seedsInDandelion)
        {
            _seeds.transform.GetChild(0).parent = _inactiveSeeds.transform;
        }

        // Move any unassigned seeds to the dandelion and arrange them around the head.
        float angle = Mathf.PI / -3.5f;

        while (_inactiveSeeds.transform.childCount > 0 && seeds.Count < _seedsInDandelion)
        {
            Transform seedXform = _inactiveSeeds.transform.GetChild(0);

            seeds.Add(seedXform);

            // To make it easier to set the initial position and orientation of the seeds, parent them to the head.
            seedXform.parent = dandelionHead;
            Vector3 offset = new Vector3(Mathf.Cos(angle), 0.0f, -Mathf.Sin(angle));
            seedXform.localPosition = offset * Random.Range(0.3f, 0.5f);
            seedXform.transform.localRotation = Quaternion.AngleAxis(angle * 180.0f / Mathf.PI - 90.0f, Vector3.up);

            // Now move them into world space so that we can animate them.
            seedXform.parent = _seeds.transform;
            Vector3 initialVelocity = (seedXform.position - dandelionHead.position).normalized;

            Seed seed = seedXform.gameObject.GetComponent<Seed>();

            seed.Eject(initialVelocity * Random.Range(2.2f, 2.8f));

            angle += Mathf.PI / 12.0f;
        }
    }
}

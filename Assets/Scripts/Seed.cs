﻿using UnityEngine;
using System.Collections;

public class Seed : MonoBehaviour
{
    DandelionField _field;
    Vector3 _velocity;
    float _startTime;
    float _lifetime;

	// Use this for initialization
	void Start() 
    {
        _field = FindObjectOfType<DandelionField>();
	}
	
	// Update is called once per frame
	void Update()
    {
        float px = _field._perlin.Noise(transform.position.x, transform.position.z, Time.time);
        float py = _field._perlin.Noise(transform.position.x, transform.position.y, Time.time);
        float pz = _field._perlin.Noise(transform.position.y, transform.position.z, Time.time);
        Vector3 pv = new Vector3(px, py, pz) * 0.1f;
        _velocity += pv;
        _velocity.y += 1.0f * Time.deltaTime;

        Vector3 oldPos = transform.position;
        Vector3 newPos = transform.position + _velocity * Time.deltaTime;
        transform.position = newPos;
        transform.rotation = Quaternion.FromToRotation((newPos - oldPos).normalized, Vector3.forward);

        if (Time.time - _startTime > _lifetime)
            _field.KillSeed(this);
	}

    public void Eject(Vector3 initialVelocity)
    {
        _velocity = initialVelocity;
        _startTime = Time.time;
        _lifetime = Random.Range(1.0f, 2.0f);
    }
}
